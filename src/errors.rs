use std::error;
use std::fmt;

pub type Result<T> = std::result::Result<T, HoraireError>;

#[derive(Debug)]
pub enum HoraireError {
    // We will defer to the parse error implementation for their error.
    // Supplying extra info requires adding more data to the type.
    Req(::reqwest::Error),
    Io(::std::io::Error),

    InvalidAnswerError,
    MissingField(String), //field name
}

impl fmt::Display for HoraireError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            HoraireError::Req(ref e) => e.fmt(f),
            HoraireError::Io(ref e) => e.fmt(f),
            HoraireError::InvalidAnswerError => write!(f, "Invalid answer from the web server"),
            HoraireError::MissingField(ref missing_field) => write!(f, "Missing field : {}", missing_field),
        }
    }
}

impl error::Error for HoraireError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            HoraireError::Req(ref e) => Some(e),
            HoraireError::Io(ref e) => Some(e),
            HoraireError::InvalidAnswerError => None,
            HoraireError::MissingField(_) => None,
        }
    }
}

// Implement the conversion from `ParseIntError` to `HoraireError`.
// This will be automatically called by `?` if a `::reqwest::Error`
// needs to be converted into a `HoraireError`.
impl From<::reqwest::Error> for HoraireError {
    fn from(err: ::reqwest::Error) -> HoraireError {
        HoraireError::Req(err)
    }
}

impl From<::std::io::Error> for HoraireError {
    fn from(err: ::std::io::Error) -> HoraireError {
        HoraireError::Io(err)
    }
}
