# rs-horaire - Retrieve timetables of trains and RER in France (rust implementation).

[![pipeline status](https://gitlab.com/prisme60/rs-horaire/badges/master/pipeline.svg)](https://gitlab.com/prisme60/rs-horaire/commits/master)

Look at the [Examples](https://github.com/prisme60/rs-horaire/tree/master/examples)

RATP data retrieval is **NOT WORKING** due to the Cloudflare protection. The request sent by *Reqwest crate* is detected as a robot, and it is not able to pass the Cloudflare challenge! I have test some "cloudflare bypasser rust code", but challenge is often updated, and the code is now obsolete.