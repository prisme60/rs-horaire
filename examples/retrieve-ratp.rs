use horaire::errors::Result;
use horaire::source::ratp::ratp;
use horaire::timelines::display_time_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let timelines = ratp("A", "Auber").await?;
    display_time_lines(timelines.iter());
    Ok(())
}
