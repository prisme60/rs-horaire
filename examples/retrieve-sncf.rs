use horaire::{errors::Result, source::sncf::sncf, timelines::display_time_lines};

#[tokio::main]
async fn main() -> Result<()> {
    display_time_lines(sncf("PSL", true).await?.iter());
    Ok(())
}
